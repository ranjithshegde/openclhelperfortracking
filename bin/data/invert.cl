__kernel void Invert(__global unsigned char* data, __global unsigned char* outData, unsigned char mouseDeg)
{
    outData[get_global_id(0)] = mouseDeg - data[get_global_id(0)];
}
