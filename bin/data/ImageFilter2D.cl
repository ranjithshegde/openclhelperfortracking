__constant sampler_t sampler = CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

__kernel void gaussian_filter(__read_only image2d_t srcImg, __write_only image2d_t dstImg, int width, int height, float threshold)
{
    float kernelWeights[9] = { 1.0f * threshold, 2.0f * threshold, 1.0f * threshold,
        2.0f * threshold, 4.0f * threshold, 2.0f * threshold,
        1.0f * threshold, 2.0f * threshold, 1.0f * threshold };

    int2 startImageCoord = (int2)(get_global_id(0) - 1, get_global_id(1) - 1);
    int2 endImageCoord = (int2)(get_global_id(0) + 1, get_global_id(1) + 1);
    int2 outImageCoord = (int2)(get_global_id(0), get_global_id(1));

    if (outImageCoord.x < width && outImageCoord.y < height) {
        int weight = 0;
        float4 outColor = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
        for (int y = startImageCoord.y; y <= endImageCoord.y; y++) {
            for (int x = startImageCoord.x; x <= endImageCoord.x; x++) {
                outColor += (read_imagef(srcImg, sampler, (int2)(x, y)) * (kernelWeights[weight] / 16));
                weight += 1;
            }
        }
        write_imagef(dstImg, outImageCoord, outColor);
    }
}

__kernel void sobel_operator(__read_only image2d_t srcImg, __write_only image2d_t dstImg, int width, int height)
{
    float Gx[9] = { -1, 0, 1, -2, 0, 2, -1, 0, 1 };

    float Gy[9] = { 1, 2, 1, 0, 0, 0, -1, -2, -1 };

    int2 startImageCoord = (int2)(get_global_id(0) - 1, get_global_id(1) - 1);
    int2 endImageCoord = (int2)(get_global_id(0) + 1, get_global_id(1) + 1);
    int2 outImageCoord = (int2)(get_global_id(0), get_global_id(1));

    if (outImageCoord.x < width && outImageCoord.y < height) {
        int weight = 0;
        int yc = 0;
        float4 outColorX = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
        float4 outColorY = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
        float4 outColor = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
        for (int y = startImageCoord.y; y <= endImageCoord.y; y++) {
            for (int x = startImageCoord.x; x <= endImageCoord.x; x++) {
                outColorX += (read_imagef(srcImg, sampler, (int2)(x, y)) * Gx[weight]);
                weight += 1;
            }
        }
        for (int y = startImageCoord.y; y <= endImageCoord.y; y++) {
            for (int x = startImageCoord.x; x <= endImageCoord.x; x++) {
                outColorY += (read_imagef(srcImg, sampler, (int2)(x, y)) * Gy[yc]);
                yc += 1;
            }
        }
        outColorX = outColorX * outColorX;
        outColorY = outColorY * outColorY;
        outColor = outColorX + outColorY;
        outColor = sqrt(outColor);
        write_imagef(dstImg, outImageCoord, outColor);
    }
}

//__kernel void testKernelWrite(__read_only image2d_t source, __write_only image2d_t resultTexture)
//{
//    int2 imgCoords = (int2)(get_global_id(0), get_global_id(1));
//
//    /* float4 imgVal = (float4)(1.0, 0.0, 0.0, 1.0); */
//    float4 imgVal = read_imagef(source, sampler, imgCoords);
//    ;
//
//    imgVal.w = 1.0 - imgVal.w;
//    imgVal.x = 1.0 - imgVal.x;
//    imgVal.y = 1.0 - imgVal.y;
//    imgVal.z = 1.0 - imgVal.z;
//
//    write_imagef(resultTexture, imgCoords, imgVal);
//}
