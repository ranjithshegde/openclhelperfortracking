#pragma once

#include <unordered_map>
#include <utility>
#define CL_HPP_TARGET_OPENCL_VERSION 300
// #define CL_HPP_ENABLE_EXCEPTIONS

#include "ofMain.h"

#include <CL/opencl.hpp>

//! Helper functions for `OpenCL` C++ API. Automate context creation, openGL hooks and dedicated graphics card selection.
//! Simple mechanisms for sharing data between `OpenGL` and `OpenCL` directly on the graphics card, whithout mid-level RAM buffer.
class CLhelper {
public:
    //! Default constructor
    CLhelper();

    //! A constructor that also creates the context, queries the platforms to select the passed device.
    //! @param card The name of the GPU vendor to select for OpenCL processing.
    CLhelper(std::string card);

    //! Same as `CLhelper::CLhelper(std::string card)`
    //! Use this function with the default constructor if you wish to instantiate the member at a later stage
    //! @param card The name of the GPU vendor to select for OpenCL processing.
    void setup(std::string card);

    //! Creates CL program. The context properties are set to be shared with the current
    //! OpenGL context if set. This function creates the context, assigns the OpenCL program code and opens a command queue,
    //! all properties available as member functions or variables.
    //! @param file Location of the `OpenCL` worker file.
    //! @param share Specifiy whether to create a shared context with `OpenGL`
    void createProgram(std::string file, bool share);

    //! Create a kernel against the specified function isnide the OpenCL program.
    //! This is essentially what is executed by OpenCL.
    //! @param Name of the function to create the kernel for.
    cl::Kernel createKernel(std::string function);

    //! Returns the currently active OpenCL device.
    [[nodiscard]] inline cl::Device getDevice() const
    {
        return m_Device;
    }

    //! Returns the current OpenCL context
    [[nodiscard]] inline cl::Context getContext() const
    {
        return m_Context;
    }

    //! Returns the currently bound kernel
    [[nodiscard]] inline cl::Kernel getKernel() const
    {
        return m_Kernel;
    }

    //! Returns the current command sequences
    [[nodiscard]] inline cl::CommandQueue getCommandQueue() const
    {
        return m_Queue;
    }

    //! Wrapper function to detect and print OpenCL errors to `stdout`
    static inline void handle_error(cl_int err, std::string message)
    {
        if (err) {
            std::cerr << message << err << std::endl;
        }
    }

    std::pair<std::string, cl::Kernel> Kernels;

private:
    cl::Platform m_Platform;
    cl::Context m_Context;
    cl::Program m_Program;
    cl_int m_Err;
    // std::vector<std::pair<std::string, cl::Kernel>> m_Kernels;
    std::unordered_map<std::string, cl::Kernel> m_Kernels;
    cl::Kernel m_Kernel;
    cl::CommandQueue m_Queue;
    cl::Device m_Device;
    cl_context_properties* m_props;
    // cl::Buffer m_inBuf;
    // cl::Buffer outBuf;
};
