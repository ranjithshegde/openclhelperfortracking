#include "clHelper.hpp"
#include <utility>

// #include "GL/glx.h"

CLhelper::CLhelper()
{
}

CLhelper::CLhelper(std::string card)
{
    cl::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);
    assert(platforms.size() > 0);

    for (cl::Platform platform : platforms) {
        std::vector<cl::Device> devices;
        std::string pName = platform.getInfo<CL_PLATFORM_NAME>();
        if (pName.find(card) != std::string::npos) {
            platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
            m_Device = devices.front();
            m_Platform = platform;
        }
    }
}

void CLhelper::setup(std::string card)
{
    cl::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);
    assert(platforms.size() > 0);

    for (cl::Platform platform : platforms) {
        std::vector<cl::Device> devices;
        std::string pName = platform.getInfo<CL_PLATFORM_NAME>();

        if (pName.find(card) != std::string::npos) {
            platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
            m_Device = devices.front();
            m_Platform = platform;
        }
    }
}

void CLhelper::createProgram(std::string file, bool share)
{
    cl_context_properties properties[] = {
        CL_GL_CONTEXT_KHR, (cl_context_properties)ofGetWindowPtr()->getGLXContext(),
        // CL_GL_CONTEXT_KHR, (cl_context_properties)glXGetCurrentContext(),
        CL_GLX_DISPLAY_KHR, (cl_context_properties)ofGetWindowPtr()->getX11Display(),
        // CL_GLX_DISPLAY_KHR, (cl_context_properties)glXGetCurrentDisplay(),
        CL_CONTEXT_PLATFORM, (cl_context_properties)m_Platform(), 0
    };

    std::ifstream kernelFile(file);
    std::string src(std::istreambuf_iterator<char>(kernelFile), (std::istreambuf_iterator<char>()));

    cl::Program::Sources sources { src };
    if (share) {
        m_Context = cl::Context(m_Device, properties, nullptr, nullptr, &m_Err);
    } else {
        m_Context = cl::Context(m_Device, nullptr, nullptr, nullptr, &m_Err);
    }
    handle_error(m_Err, "Error creating context: ");

    m_Program = cl::Program(m_Context, sources, &m_Err);

    handle_error(m_Err, "Error creating Program: ");

    m_Err = m_Program.build();
    handle_error(m_Err, "Error building Program: ");
    // try {
    //     m_Program.build();
    // } catch (cl::BuildError const& e) {
    //     std::cout << " Error building: " << m_Program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(m_Device) << endl;
    //     // std::cout << " Error building: " << endl;
    //     cl::BuildLogType buildLog = e.getBuildLog();
    //     for (auto line : buildLog) {
    //         std::cout << "\t" << line.first.getInfo<CL_DEVICE_NAME>() << "=>" << line.second << endl;
    //     }
    //     throw std::runtime_error("");
    // }

    m_Queue = cl::CommandQueue(m_Context, m_Device, 0, &m_Err);
    handle_error(m_Err, "Error creating Queue: ");
}

cl::Kernel CLhelper::createKernel(std::string function)
{
    m_Kernels[function] = cl::Kernel(m_Program, function.c_str(), &m_Err);
    handle_error(m_Err, "Error building Kernel: ");

    return m_Kernels[function];
}
