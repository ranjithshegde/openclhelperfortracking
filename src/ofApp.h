#pragma once

#include "clHelper.hpp"

//! Simple Gaussian blur implementation. It shows how to share a context between
//! OpenCL and OpenGL, and how to pass OpenGL data to OpenCL and back.
class ofApp : public ofBaseApp {

public:
    void setup();
    void update();
    void draw();
    void exit();

    void mouseMoved(int x, int y);
    void keyPressed(int key);

    //! Helper class to interface with `GStreamer`. This function provides a
    //! handler for hardware camera.
    ofVideoGrabber grabber;

    CLhelper CL;
    cl_int err;
    cl::Context context;
    cl::Kernel gaussian, sobel;
    cl::CommandQueue queue;
    cl::Buffer inBuf;
    cl::Buffer outBuf;
    cl::Device device;
    cl::Event AcqEvent, KernelEvent[2], WriteEvent;
    cl::ImageGL inTex, outTex;

    //! OpenGL texture wrapper
    ofTexture tex;
    //! Simple pixel buffer with an attached openGL texture
    ofImage img;
    std::vector<cl::Memory> texV;
    GLuint ipbo, opbo;
    int width, height;
    float threshold;
    bool to_sobel = false;
};
