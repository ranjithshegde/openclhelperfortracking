#include "ofApp.h"

//--------------------------------------------------------------
//! Creates OpenCL context, allocates memory and assigns the kernel
void ofApp::setup()
{
    CL.setup("NVID");
    CL.createProgram(ofToDataPath("ImageFilter2D.cl", true), true);
    context = CL.getContext();

    threshold = 5.0f;
    width = 1920;
    height = 1080;
    grabber.setup(width, height);

    tex.allocate(width, height, GL_RGB8);

    ipbo = grabber.getTexture().getTextureData().textureID;
    opbo = tex.getTextureData().textureID;

    inTex = cl::ImageGL(context, CL_MEM_READ_ONLY, grabber.getTexture().getTextureData().textureTarget, 0, ipbo, &err);
    CL.handle_error(err, "InTexture: ");

    outTex = cl::ImageGL(context, CL_MEM_READ_WRITE, tex.getTextureData().textureTarget, 0, opbo, &err);
    CL.handle_error(err, "OutTexture: ");

    gaussian = CL.createKernel("gaussian_filter");
    sobel = CL.createKernel("sobel_operator");
    // kernel = CL.createKernel("testKernelWrite");

    err = gaussian.setArg(0, inTex);
    CL.handle_error(err, "Arg 0 ");
    err = gaussian.setArg(1, outTex);
    CL.handle_error(err, "Arg 1 ");
    err = gaussian.setArg(2, width);
    CL.handle_error(err, "Arg 2 ");
    err = gaussian.setArg(3, height);
    CL.handle_error(err, "Arg 3 ");
    err = gaussian.setArg(4, threshold);
    CL.handle_error(err, "Arg 4 ");

    err = sobel.setArg(0, outTex);
    CL.handle_error(err, "Arg 0 ");
    err = sobel.setArg(1, outTex);
    CL.handle_error(err, "Arg 1 ");
    err = sobel.setArg(2, width);
    CL.handle_error(err, "Arg 2 ");
    err = sobel.setArg(3, height);
    CL.handle_error(err, "Arg 3 ");

    queue = CL.getCommandQueue();

    texV.push_back(inTex);
    texV.push_back(outTex);
}

//--------------------------------------------------------------
//! This function demonstrates the interoperation of OpenCL and OpenGL.
//! `cl::CommandQueue::AcquireGLObjects` gets the currently bound texture.
//! `cl::CommandQueue::enqueueNDRangeKernel` performs the OpenCL operations on
//! the acquired data (Gaussian blur).
//! `cl::CommandQueue::enqueueReleaseGLObjects` sends the processed texture back
//! to OpenGL

void ofApp::update()
{
    grabber.update();
    glFinish();

    err = queue.enqueueAcquireGLObjects(&texV, nullptr, &AcqEvent);
    CL.handle_error(err, "WriteGL ");

    err = AcqEvent.wait();
    CL.handle_error(err, "Aquire Wait ");

    err = queue.enqueueNDRangeKernel(gaussian, cl::NullRange, cl::NDRange(width, height), cl::NullRange, nullptr, &KernelEvent[0]);
    CL.handle_error(err, "Gaussian Kernel ");
    err = KernelEvent[0].wait();
    CL.handle_error(err, "Gaussian Kernel Wait");

    if (to_sobel) {
        err = queue.enqueueNDRangeKernel(sobel, cl::NullRange, cl::NDRange(width, height), cl::NullRange, nullptr, &KernelEvent[1]);
        CL.handle_error(err, "Sobel Kernel ");
        err = KernelEvent[1].wait();
        CL.handle_error(err, "Sobel Kernel Wait");
    }

    queue.enqueueReleaseGLObjects(&texV, nullptr, &WriteEvent);
    CL.handle_error(err, "ReadGL ");
    err = WriteEvent.wait();
    CL.handle_error(err, "Release Wait ");
}

//--------------------------------------------------------------
void ofApp::draw()
{
    tex.draw(0, 0);
}

//--------------------------------------------------------------
//! Mouse position sets the threshold for Gaussian blur.
void ofApp::mouseMoved(int x, int y)
{
    threshold = ofMap((float)x, 0.f, (float)ofGetWidth(), 1.f, 10.f);
    err = gaussian.setArg(4, threshold);
    CL.handle_error(err, "Arg 4 ");
}

//--------------------------------------------------------------

void ofApp::keyPressed(int key)
{
    switch (key) {
    case ' ': {
        to_sobel = !to_sobel;
    } break;
    default:
        break;
    }
}

//--------------------------------------------------------------
//! Close openCL context and program on exit
void ofApp::exit()
{
    // soundStream.close();

    clReleaseKernel(gaussian());
    clReleaseCommandQueue(queue());
    // clReleaseProgram(program());
    clReleaseContext(context());
}
