# Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`class `[`CLhelper`](#classCLhelper) | Helper functions for `OpenCL` C++ API. Automate context creation, openGL hooks and dedicated graphics card selection. Simple mechanisms for sharing data between `OpenGL` and `OpenCL` directly on the graphics card, whithout mid-level RAM buffer.
`class `[`ofApp`](#classofApp) | Simple Gaussian blur implementation. It shows how to share a context between OpenCL and OpenGL, and how to pass OpenGL data to OpenCL and back.

# class `CLhelper` 

Helper functions for `OpenCL` C++ API. Automate context creation, openGL hooks and dedicated graphics card selection. Simple mechanisms for sharing data between `OpenGL` and `OpenCL` directly on the graphics card, whithout mid-level RAM buffer.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public  `[`CLhelper`](#classCLhelper_1a8587158cfc532ccbe383e91216f115bb)`()` | Default constructor.
`public  `[`CLhelper`](#classCLhelper_1a256cdadf5bdedc69fadbb53a22d5f19e)`(std::string card)` | A constructor that also creates the context, queries the platforms to select the passed device. 
`public void `[`setup`](#classCLhelper_1a2753cc11d0d21b262101697543f3eed4)`(std::string card)` | Same as `[CLhelper::CLhelper(std::string card)](#classCLhelper_1a256cdadf5bdedc69fadbb53a22d5f19e)` Use this function with the default constructor if you wish to instantiate the member at a later stage 
`public void `[`createProgram`](#classCLhelper_1afedc36b88be09fcd504cd0d6a447eead)`(std::string file,bool share)` | Creates CL program. The context properties are set to be shared with the current OpenGL context if set. This function creates the context, assigns the OpenCL program code and opens a command queue, all properties available as member functions or variables. 
`public cl::Kernel `[`createKernel`](#classCLhelper_1a168f57bfecdfbf6e53df6a4c20051c12)`(std::string function)` | Create a kernel against the specified function isnide the OpenCL program. This is essentially what is executed by OpenCL. 
`public inline cl::Device `[`getDevice`](#classCLhelper_1a02b0c298b85e23bcea4c865ca627f574)`() const` | Returns the currently active OpenCL device.
`public inline cl::Context `[`getContext`](#classCLhelper_1a6b25be5d3ea4a913c12f86dd0b1c3307)`() const` | Returns the current OpenCL context.
`public inline cl::Kernel `[`getKernel`](#classCLhelper_1acbceff4bad2a16cd02b909705d19a911)`() const` | Returns the currently bound kernel.
`public inline cl::CommandQueue `[`getCommandQueue`](#classCLhelper_1a68b9a137ae0c91e7fff98d94a502fd0d)`() const` | Returns the current command sequences.

## Members

#### `public  `[`CLhelper`](#classCLhelper_1a8587158cfc532ccbe383e91216f115bb)`()` 

Default constructor.

#### `public  `[`CLhelper`](#classCLhelper_1a256cdadf5bdedc69fadbb53a22d5f19e)`(std::string card)` 

A constructor that also creates the context, queries the platforms to select the passed device. 
#### Parameters
* `card` The name of the GPU vendor to select for OpenCL processing.

#### `public void `[`setup`](#classCLhelper_1a2753cc11d0d21b262101697543f3eed4)`(std::string card)` 

Same as `[CLhelper::CLhelper(std::string card)](#classCLhelper_1a256cdadf5bdedc69fadbb53a22d5f19e)` Use this function with the default constructor if you wish to instantiate the member at a later stage 
#### Parameters
* `card` The name of the GPU vendor to select for OpenCL processing.

#### `public void `[`createProgram`](#classCLhelper_1afedc36b88be09fcd504cd0d6a447eead)`(std::string file,bool share)` 

Creates CL program. The context properties are set to be shared with the current OpenGL context if set. This function creates the context, assigns the OpenCL program code and opens a command queue, all properties available as member functions or variables. 
#### Parameters
* `file` Location of the `OpenCL` worker file. 

* `share` Specifiy whether to create a shared context with `OpenGL`

#### `public cl::Kernel `[`createKernel`](#classCLhelper_1a168f57bfecdfbf6e53df6a4c20051c12)`(std::string function)` 

Create a kernel against the specified function isnide the OpenCL program. This is essentially what is executed by OpenCL. 
#### Parameters
* `Name` of the function to create the kernel for.

#### `public inline cl::Device `[`getDevice`](#classCLhelper_1a02b0c298b85e23bcea4c865ca627f574)`() const` 

Returns the currently active OpenCL device.

#### `public inline cl::Context `[`getContext`](#classCLhelper_1a6b25be5d3ea4a913c12f86dd0b1c3307)`() const` 

Returns the current OpenCL context.

#### `public inline cl::Kernel `[`getKernel`](#classCLhelper_1acbceff4bad2a16cd02b909705d19a911)`() const` 

Returns the currently bound kernel.

#### `public inline cl::CommandQueue `[`getCommandQueue`](#classCLhelper_1a68b9a137ae0c91e7fff98d94a502fd0d)`() const` 

Returns the current command sequences.

# class `ofApp` 

```
class ofApp
  : public ofBaseApp
```  

Simple Gaussian blur implementation. It shows how to share a context between OpenCL and OpenGL, and how to pass OpenGL data to OpenCL and back.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public ofVideoGrabber `[`grabber`](#classofApp_1a189406d3ccd6ed7977677541239f0ab5) | Helper class to interface with `GStreamer`. This function provides a handler for hardware camera.
`public `[`CLhelper`](#classCLhelper)` `[`CL`](#classofApp_1ab9315c98cd364dbc00e1264fd6ffb980) | 
`public cl_int `[`err`](#classofApp_1a80833dd277da7f36fea13da5daa43f13) | 
`public cl::Context `[`context`](#classofApp_1a6244883bbb878721060ccb9ac3f7ca51) | 
`public cl::Kernel `[`kernel`](#classofApp_1aa81ef1b65a994246a7d6737cac89e101) | 
`public cl::CommandQueue `[`queue`](#classofApp_1a8410dd5e9cad51b18b8cdc16c17e09a9) | 
`public cl::Buffer `[`inBuf`](#classofApp_1a4b800ffd04c1fea735bd20ac3c026ad1) | 
`public cl::Buffer `[`outBuf`](#classofApp_1aff184970a7a86bc0bd71286acbf86ce4) | 
`public cl::Device `[`device`](#classofApp_1a3a7a1c89e39699085bc8ca9a82517f40) | 
`public cl::Event `[`AcqEvent`](#classofApp_1a86a2cadd876d1f670e859f4820b7ec18) | 
`public cl::Event `[`KernelEvent`](#classofApp_1a77e41319963e56c77c6786269796e4bf) | 
`public cl::Event `[`WriteEvent`](#classofApp_1a4a87dead20f2e437fbee1364aad9edc0) | 
`public cl::ImageGL `[`inTex`](#classofApp_1a5d39fc432c520af4ae610dbb625d23f7) | 
`public cl::ImageGL `[`outTex`](#classofApp_1a056581d7c79252b41e12454d7a293ea5) | 
`public ofTexture `[`tex`](#classofApp_1a4abdf91455fbbc69b90a306e0b833d99) | OpenGL texture wrapper.
`public ofImage `[`img`](#classofApp_1a3a87110a3314e9da3a0222f73431a625) | Simple pixel buffer with an attached openGL texture.
`public std::vector< cl::Memory > `[`texV`](#classofApp_1a6f5314cdcd14ce98fc3d0a992364f468) | 
`public GLuint `[`ipbo`](#classofApp_1ac565b220b9da5721c9d06499ebb644ab) | 
`public GLuint `[`opbo`](#classofApp_1ab2b5a042c13c5dc99c3c306ecacd4452) | 
`public int `[`width`](#classofApp_1ae4389d178073fec79b639aac0499be4b) | 
`public int `[`height`](#classofApp_1af5a0d1afd81e92ac9ba9cdaa222cf180) | 
`public float `[`threshold`](#classofApp_1a34bdeaeccda47dcdb12a116ee1b5b00b) | 
`public void `[`setup`](#classofApp_1af68eaa1366244f7a541cd08e02199c12)`()` | Creates OpenCL context, allocates memory and assigns the kernel.
`public void `[`update`](#classofApp_1afef41ea4aee5a22ea530afba33ae7a7b)`()` | This function demonstrates the interoperation of OpenCL and OpenGL. `cl::CommandQueue::AcquireGLObjects` gets the currently bound texture. `cl::CommandQueue::enqueueNDRangeKernel` performs the OpenCL operations on the acquired data (Gaussian blur). `cl::CommandQueue::enqueueReleaseGLObjects` sends the processed texture back to OpenGL
`public void `[`draw`](#classofApp_1a75dd45437b9e317db73d8daef1ad49f8)`()` | 
`public void `[`exit`](#classofApp_1a41588341bbe9be134f6abdc2eb7cfd4c)`()` | Close openCL context and program on exit.
`public void `[`mouseMoved`](#classofApp_1a158b41a606310db4633fdb817b21047c)`(int x,int y)` | Mouse position sets the threshold for Gaussian blur.

## Members

#### `public ofVideoGrabber `[`grabber`](#classofApp_1a189406d3ccd6ed7977677541239f0ab5) 

Helper class to interface with `GStreamer`. This function provides a handler for hardware camera.

#### `public `[`CLhelper`](#classCLhelper)` `[`CL`](#classofApp_1ab9315c98cd364dbc00e1264fd6ffb980) 

#### `public cl_int `[`err`](#classofApp_1a80833dd277da7f36fea13da5daa43f13) 

#### `public cl::Context `[`context`](#classofApp_1a6244883bbb878721060ccb9ac3f7ca51) 

#### `public cl::Kernel `[`kernel`](#classofApp_1aa81ef1b65a994246a7d6737cac89e101) 

#### `public cl::CommandQueue `[`queue`](#classofApp_1a8410dd5e9cad51b18b8cdc16c17e09a9) 

#### `public cl::Buffer `[`inBuf`](#classofApp_1a4b800ffd04c1fea735bd20ac3c026ad1) 

#### `public cl::Buffer `[`outBuf`](#classofApp_1aff184970a7a86bc0bd71286acbf86ce4) 

#### `public cl::Device `[`device`](#classofApp_1a3a7a1c89e39699085bc8ca9a82517f40) 

#### `public cl::Event `[`AcqEvent`](#classofApp_1a86a2cadd876d1f670e859f4820b7ec18) 

#### `public cl::Event `[`KernelEvent`](#classofApp_1a77e41319963e56c77c6786269796e4bf) 

#### `public cl::Event `[`WriteEvent`](#classofApp_1a4a87dead20f2e437fbee1364aad9edc0) 

#### `public cl::ImageGL `[`inTex`](#classofApp_1a5d39fc432c520af4ae610dbb625d23f7) 

#### `public cl::ImageGL `[`outTex`](#classofApp_1a056581d7c79252b41e12454d7a293ea5) 

#### `public ofTexture `[`tex`](#classofApp_1a4abdf91455fbbc69b90a306e0b833d99) 

OpenGL texture wrapper.

#### `public ofImage `[`img`](#classofApp_1a3a87110a3314e9da3a0222f73431a625) 

Simple pixel buffer with an attached openGL texture.

#### `public std::vector< cl::Memory > `[`texV`](#classofApp_1a6f5314cdcd14ce98fc3d0a992364f468) 

#### `public GLuint `[`ipbo`](#classofApp_1ac565b220b9da5721c9d06499ebb644ab) 

#### `public GLuint `[`opbo`](#classofApp_1ab2b5a042c13c5dc99c3c306ecacd4452) 

#### `public int `[`width`](#classofApp_1ae4389d178073fec79b639aac0499be4b) 

#### `public int `[`height`](#classofApp_1af5a0d1afd81e92ac9ba9cdaa222cf180) 

#### `public float `[`threshold`](#classofApp_1a34bdeaeccda47dcdb12a116ee1b5b00b) 

#### `public void `[`setup`](#classofApp_1af68eaa1366244f7a541cd08e02199c12)`()` 

Creates OpenCL context, allocates memory and assigns the kernel.

#### `public void `[`update`](#classofApp_1afef41ea4aee5a22ea530afba33ae7a7b)`()` 

This function demonstrates the interoperation of OpenCL and OpenGL. `cl::CommandQueue::AcquireGLObjects` gets the currently bound texture. `cl::CommandQueue::enqueueNDRangeKernel` performs the OpenCL operations on the acquired data (Gaussian blur). `cl::CommandQueue::enqueueReleaseGLObjects` sends the processed texture back to OpenGL

#### `public void `[`draw`](#classofApp_1a75dd45437b9e317db73d8daef1ad49f8)`()` 

#### `public void `[`exit`](#classofApp_1a41588341bbe9be134f6abdc2eb7cfd4c)`()` 

Close openCL context and program on exit.

#### `public void `[`mouseMoved`](#classofApp_1a158b41a606310db4633fdb817b21047c)`(int x,int y)` 

Mouse position sets the threshold for Gaussian blur.

Generated by [Moxygen](https://sourcey.com/moxygen)